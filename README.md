适用于OpenHarmony3.0LTS，在[xfan1024](https://gitee.com/xfan1024/oh-dropbear)为树莓派移植的基础上整理。

## 使用教程

首先要下载，有两种办法

### 方法1
在`.repo/manifests/default.xml`文件下添加如下语句：

```xml
  <remote fetch="https://gitlab.com/czyhit/" name="czyhit" review="https://gitlab.com/czyhit/"/>

  <project name="dropbear_ohos" path="third_party/dropbear" revision="master"  remote="czyhit">
      <linkfile src="openharmony-develtools" dest="developtools/dropbear"/>
  </project>
```

然后执行命令：

```bash
repo sync -c third_party/dropbear
```
### 方法2
直接将本仓库代码打包下载后，解压放入`//third_party/dropbear`，然后创建链接文件`developtools/dropbear`到`//third_party/dropbear/openharmony-develtools`

下载完后加入到编译配置中

在`productdefine/common/products/Hi3516DV300.json`下加入

```json
"developtools:dropbear": {}
```

然后下次编译镜像的时候就会编译进去，开机后自动启动

默认用户名为root

默认密码为123456


__________________________________________________________________________________________________

This is Dropbear, a smallish SSH server and client.
https://matt.ucc.asn.au/dropbear/dropbear.html

INSTALL has compilation instructions.

MULTI has instructions on making a multi-purpose binary (ie a single binary
which performs multiple tasks, to save disk space)

SMALL has some tips on creating small binaries.

Please contact me if you have any questions/bugs found/features/ideas/comments etc :)
There is also a mailing list http://lists.ucc.gu.uwa.edu.au/mailman/listinfo/dropbear

Matt Johnston
matt@ucc.asn.au


In the absence of detailed documentation, some notes follow:
============================================================================

Server public key auth:

You can use ~/.ssh/authorized_keys in the same way as with OpenSSH, just put
the key entries in that file. They should be of the form:

ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAIEAwVa6M6cGVmUcLl2cFzkxEoJd06Ub4bVDsYrWvXhvUV+ZAM9uGuewZBDoAqNKJxoIn0Hyd0Nk/yU99UVv6NWV/5YSHtnf35LKds56j7cuzoQpFIdjNwdxAN0PCET/MG8qyskG/2IE2DPNIaJ3Wy+Ws4IZEgdJgPlTYUBWWtCWOGc= someone@hostname

You must make sure that ~/.ssh, and the key file, are only writable by the
user. Beware of editors that split the key into multiple lines.

Dropbear supports some options for authorized_keys entries, see the manpage.

============================================================================

Client public key auth:

Dropbear can do public key auth as a client, but you will have to convert
OpenSSH style keys to Dropbear format, or use dropbearkey to create them.

If you have an OpenSSH-style private key ~/.ssh/id_rsa, you need to do:

dropbearconvert openssh dropbear ~/.ssh/id_rsa  ~/.ssh/id_rsa.db
dbclient -i ~/.ssh/id_rsa.db <hostname>

Dropbear does not support encrypted hostkeys though can connect to ssh-agent.

============================================================================

If you want to get the public-key portion of a Dropbear private key, look at
dropbearkey's '-y' option.

============================================================================

To run the server, you need to generate server keys, this is one-off:
./dropbearkey -t rsa -f dropbear_rsa_host_key
./dropbearkey -t dss -f dropbear_dss_host_key
./dropbearkey -t ecdsa -f dropbear_ecdsa_host_key
./dropbearkey -t ed25519 -f dropbear_ed25519_host_key

or alternatively convert OpenSSH keys to Dropbear:
./dropbearconvert openssh dropbear /etc/ssh/ssh_host_dsa_key dropbear_dss_host_key

You can also get Dropbear to create keys when the first connection is made -
this is preferable to generating keys when the system boots. Make sure 
/etc/dropbear/ exists and then pass '-R' to the dropbear server.

============================================================================

If the server is run as non-root, you most likely won't be able to allocate a
pty, and you cannot login as any user other than that running the daemon
(obviously). Shadow passwords will also be unusable as non-root.

============================================================================

The Dropbear distribution includes a standalone version of OpenSSH's scp
program. You can compile it with "make scp", you may want to change the path
of the ssh binary, specified by _PATH_SSH_PROGRAM in options.h . By default
the progress meter isn't compiled in to save space, you can enable it by 
adding 'SCPPROGRESS=1' to the make commandline.
